#Installation
```
Créer une nouvelle base de donnée
Créer un fichier .env se basant sur le fichier .env.example
```

#### Importation de la bdd
```
ouvrir une invite de commande, aller dans le projet puis executer la commande :
php artisan migrate
```

#### Peuplement de la bdd
```
Executer le script de peuplement 'Peuplement.sql' qui est dans le dossier BDD dans la bdd
```

#### En cas de problème
```
En cas de problème d'importation ou de peuplement executer le script 'backup.sql' qui est dans le dossier BDD.
Il permet de créer la base de donnée avec des données.
```

# Routes 

### Login
```
/login
```

### Résultats
```
Page de résultat
/resultats
```

### Routes permetant d'avoir tous les éléments d'une table
```
/nomTable/All
```