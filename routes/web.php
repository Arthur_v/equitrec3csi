<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/resultats', 'ResultatGeneralController@index');

$router->get('/login', 'JuresController@login');

//Format de route :
//$router->get('/NomRoute', 'NomController@NomFunction');

$router->get('/Dossards/All', 'DossardsController@getAllDossards');
$router->get('/Dispositif/All', 'DispositifController@getAllDispositifs');
$router->get('/Epreuves/All', 'EpreuvesController@getAllEpreuves');
$router->get('/Jures/All', 'JuresController@getAllJures');
$router->get('/Niveau/All', 'NiveauController@getAllNiveaux');
$router->get('/ResultatGeneral/All', 'ResultatGeneralController@getAllResultatGeneral');
