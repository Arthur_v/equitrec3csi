<?php
/**
 * Created by PhpStorm.
 * User: avill
 * Date: 06/06/2019
 * Time: 12:49
 */

?>

<html>
<head>
    <title>Résultats</title>
    <link rel="stylesheet" href="{{ url('/bootstrap/bootstrap.min.css') }}" crossorigin="anonymous">
    <script src="{{ url('/bootstrap/jquery-3.3.1.slim.min.js') }}" crossorigin="anonymous"></script>
    <script src="{{ url('/bootstrap/popper.min.js') }}" crossorigin="anonymous"></script>
    <script src="{{ url('/bootstrap/bootstrap.min.js') }}" crossorigin="anonymous"></script>
</head>
<body>
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">n° dossard</th>
        <th scope="col">nom epreuve</th>
        <th scope="col">résultat</th>
        <th scope="col">date</th>
        <th scope="col">observation</th>
    </tr>
    </thead>
    <tbody>

    @forelse($ResultatGeneral as $value)
        <tr>
            <th scope="row">{{ $value->dossard->numero }}</th>
            <td>{{ $value->epreuve->label }}</td>
            <td>{{ $value->resultat }}</td>
            <td>{{ $value->date }}</td>
            <td>{{ $value->observation }}</td>
        </tr>
    @empty
        Aucun Résultats dans la base
    @endforelse

    </tbody>
</table>
</body>
</html>
