-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 22, 2019 at 12:08 PM
-- Server version: 5.7.14
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `equitrec_v4`
--
CREATE DATABASE IF NOT EXISTS `equitrec_v4` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `equitrec_v4`;

-- --------------------------------------------------------

--
-- Table structure for table `dispositifs`
--

CREATE TABLE IF NOT EXISTS `dispositifs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dispositifs`
--

INSERT INTO `dispositifs` (`id`, `label`) VALUES
(1, 'dispo test');

-- --------------------------------------------------------

--
-- Table structure for table `dossards`
--

CREATE TABLE IF NOT EXISTS `dossards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `niveau_id` int(11) NOT NULL,
  `numero` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `niveau_id` (`niveau_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dossards`
--

INSERT INTO `dossards` (`id`, `niveau_id`, `numero`) VALUES
(2, 1, 0),
(3, 2, 2),
(4, 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `epreuves`
--

CREATE TABLE IF NOT EXISTS `epreuves` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(50) NOT NULL,
  `dispositif_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `dispositif_id` (`dispositif_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `epreuves`
--

INSERT INTO `epreuves` (`id`, `label`, `dispositif_id`) VALUES
(1, 'epreuve de test', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jures`
--

CREATE TABLE IF NOT EXISTS `jures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `login` varchar(50) NOT NULL,
  `mdp` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jures`
--

INSERT INTO `jures` (`id`, `nom`, `prenom`, `login`, `mdp`) VALUES
(1, 'Martin', 'Matin', 'MM', '$2y$10$dES/dNz/FwEaJc/o/olBr.FAitqXvlxGRuuIX6xDMX4wb/bknhq7O');

-- --------------------------------------------------------

--
-- Table structure for table `niveaux`
--

CREATE TABLE IF NOT EXISTS `niveaux` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `niveaux`
--

INSERT INTO `niveaux` (`id`, `label`) VALUES
(1, 'nouveau niveau'),
(2, 'Niveau 2');

-- --------------------------------------------------------

--
-- Table structure for table `resultat_generals`
--

CREATE TABLE IF NOT EXISTS `resultat_generals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `epreuve_id` int(11) NOT NULL,
  `dossard_id` int(11) NOT NULL,
  `jure_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `observation` text NOT NULL,
  `resultat` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_dossards` (`dossard_id`),
  KEY `id_jures` (`jure_id`),
  KEY `id_epreuves` (`epreuve_id`),
  KEY `id_cavalier` (`epreuve_id`,`dossard_id`,`jure_id`),
  KEY `id_epreuves_2` (`epreuve_id`,`dossard_id`,`jure_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `resultat_generals`
--

INSERT INTO `resultat_generals` (`id`, `epreuve_id`, `dossard_id`, `jure_id`, `date`, `observation`, `resultat`) VALUES
(1, 1, 2, 1, '2019-06-05', 'C\'est une observation', 110),
(2, 1, 2, 1, '2019-05-16', 'sdfkqslkf', 250);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `dossards`
--
ALTER TABLE `dossards`
  ADD CONSTRAINT `dossards_ibfk_1` FOREIGN KEY (`niveau_id`) REFERENCES `niveaux` (`id`);

--
-- Constraints for table `epreuves`
--
ALTER TABLE `epreuves`
  ADD CONSTRAINT `epreuves_ibfk_1` FOREIGN KEY (`dispositif_id`) REFERENCES `dispositifs` (`id`);

--
-- Constraints for table `resultat_generals`
--
ALTER TABLE `resultat_generals`
  ADD CONSTRAINT `resultat_generals_ibfk_1` FOREIGN KEY (`dossard_id`) REFERENCES `dossards` (`id`),
  ADD CONSTRAINT `resultat_generals_ibfk_3` FOREIGN KEY (`jure_id`) REFERENCES `jures` (`id`),
  ADD CONSTRAINT `resultat_generals_ibfk_4` FOREIGN KEY (`epreuve_id`) REFERENCES `epreuves` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
