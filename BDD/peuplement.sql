INSERT INTO `jures` (`id`, `nom`, `prenom`, `login`, `mdp`) VALUES
(1, 'Martin', 'Matin', 'log', '$2y$10$dES/dNz/FwEaJc/o/olBr.FAitqXvlxGRuuIX6xDMX4wb/bknhq7O');

INSERT INTO `dispositifs` (`id`, `label`) VALUES
(1, 'dispo test');

INSERT INTO `epreuves` (`id`, `label`, `dispositif_id`) VALUES
(1, 'epreuve de test', 1);

INSERT INTO `niveaux` (`id`, `label`) VALUES
(1, 'nouveau niveau'),
(2, 'Niveau 2');

INSERT INTO `dossards` (`id`, `niveau_id`, `numero`) VALUES
(2, 1, 0),
(3, 2, 2),
(4, 1, 4);

INSERT INTO `resultat_generals` (`id`, `epreuve_id`, `dossard_id`, `jure_id`, `date`, `observation`, `resultat`) VALUES
(1, 1, 2, 1, '2019-06-05', 'C\'est une observation', 110),
(2, 1, 2, 1, '2019-05-16', 'Observation de l\'épreuve', 250);