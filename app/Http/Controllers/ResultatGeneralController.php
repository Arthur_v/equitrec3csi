<?php

namespace App\Http\Controllers;

use App\ResultatGeneral;

class ResultatGeneralController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function getAllResultatGeneral(){
        $ResultatGeneral = ResultatGeneral::all();
        return $ResultatGeneral;
    }

    public function insertResultatGeneral(){

    }

    public function index(){
        $ResultatGeneral = ResultatGeneral::with('epreuve', 'dossard')->orderBy('resultat', 'DESC')->get();
        return view('resultats', compact('ResultatGeneral'));
    }
}
