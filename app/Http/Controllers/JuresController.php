<?php

namespace App\Http\Controllers;

use App\Jures;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class JuresController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function getAllJures(){
        $Jures = Jures::all();
        return $Jures;
    }

    public function login(Request $request)
    {

        $this->validate($request, [
            'login' => 'required',
            'mdp' => 'required'
        ]);

        $jure = Jures::where('login', $request->input('login'))->first();

        $headers = [
            'Access-Control-Allow-Origin'      => '*',
            'Access-Control-Allow-Methods'     => 'GET',
            'Access-Control-Allow-Credentials' => 'true',
            'Access-Control-Max-Age'           => '86400',
            'Access-Control-Allow-Headers'     => 'Content-Type, Authorization, X-Requested-With'
        ];

        if(Hash::check($request->input('mdp'), $jure->mdp)){
            return response()->json(['status' => true], 200, $headers);
        }else{
            return response()->json(['status' => false],401, $headers);
        }
    }
}