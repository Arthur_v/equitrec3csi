<?php

namespace App\Http\Controllers;

use App\Dispositif;

class DispositifController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function getAllDispositifs(){
        $Dispositifs = Dispositif::all();
        return $Dispositifs;
    }
}
