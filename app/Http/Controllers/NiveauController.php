<?php

namespace App\Http\Controllers;

use App\Niveau;

class NiveauController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function getAllNiveaux(){
        $Niveau = Niveau::all();
        return $Niveau;
    }

}
