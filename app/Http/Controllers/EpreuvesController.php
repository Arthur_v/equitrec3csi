<?php

namespace App\Http\Controllers;

use App\Epreuves;

class EpreuvesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function getAllEpreuves(){
     $Epreuves = Epreuves::all();
     return $Epreuves;
    }
}
