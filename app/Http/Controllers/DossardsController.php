<?php

namespace App\Http\Controllers;

use App\Dossards;

class DossardsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getAllDossards(){
        $Dossards = Dossards::all();
        return $Dossards;
    }
}
