<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class ResultatGeneral extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'id_epreuves', 'id_dossards', 'id_jures', 'date', 'resultat', 'observation'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function epreuve(){
        return $this->belongsTo('App\Epreuves');
    }

    public function dossard(){
        return $this->belongsTo('App\Dossards');
    }

    public function jure(){
        return $this->belongsTo('App\Jures');
    }
}
