<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ResultatGenerals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resultat_generals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dossard_id')->unsigned()->index();
            $table->integer('jure_id')->unsigned()->index();
            $table->integer('epreuve_id')->unsigned()->index();
            $table->string('observation');
            $table->integer('resultat');
            $table->date('date');
            $table->foreign('dossard_id')->references('id')->on('dossards')->onDelete('cascade');
            $table->foreign('jure_id')->references('id')->on('jures')->onDelete('cascade');
            $table->foreign('epreuve_id')->references('id')->on('epreuves')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resultat_generals');
    }
}
